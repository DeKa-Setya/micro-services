const http = require('http');
const express = require('express');
const path = require('path');

http.createServer(function (req, res) {
    if(req.url === "/"){
        
        res.writeHead(200, {"Content-Type": "text/json"});
        res.end();
    }
    else{
        res.writeHead(404, {"Content-Type": "text/html"});
        res.end("No Page Found");
    }
}).listen(8000);
